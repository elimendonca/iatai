<?php
/*
Template Name: Single Album
*/
?>
<?php get_header(); ?>
<div id="conteudo2">
<div id="conteudo_interno" class="esquerda">
	<?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>'); } ?>

	<?php 
		while ( have_posts() ) : the_post();
		$postID = $post->ID;
		$custom = get_post_custom($post->ID);
		$albumembed_meta = $custom["albumembed_meta"][0];
	?>
	<h2 class="titulo m-top30">
		<?php the_title(); ?>
	</h2>
	<div class="share_meta m-top10 m-bottom15">
		<div class="share esquerda">
			<?php if(function_exists('kc_add_social_share')) kc_add_social_share(); ?>
		</div>
		<div class="meta direita">
			<?php the_date();?>
		</div>
	</div>
	<div class="post clear">
		<div class="entry-content m-top15">
			<?php the_content(); ?>
		</div>
		<div class="gallery_embed"><?php echo do_shortcode('[fbphotos id="'. getFBID( $albumembed_meta ) .'" txtfb="veja essa galeria completa no facebook"]'); ?></div>
		<?php endwhile; ?>
	</div>
	<?php comments_template(); ?>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
