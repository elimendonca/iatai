<?php
/*
Template Name: Index 
*/
?>
<?php get_header(); ?>
			<div class="slideshow_video m-bottom30">
				<div id="slideshow" class="esquerda"><?php dynamic_sidebar('Slider'); ?></div>
				<div id="video" class="direita"><?php dynamic_sidebar('VideoDestaque'); ?></div>
			</div>
			<div class="clear"></div>
			<?php if (is_active_sidebar('twitter-home')) {?>
			<div id="twitter" class="wrap m-bottom25"><span class="icon1 icon-twitter "></span><?php dynamic_sidebar('Twitter Home'); ?><a href="http://twitter.com/iatai" target="_blank" class="siganos direita">SIGA-NOS</a></div>
			<div class="clear"></div>
			<?php }?>
			<div id="noticias" class="esquerda m-bottom40">
				<h2>Últimas Notícias</h2>
				<div class="divisor m-bottom15"></div>
				<?php 
				$args = array( 'numberposts' => 4, 'posts_per_page' => 4, 'orderby' => 'date', 'order' => 'DESC' );
				$loop = new WP_Query( $args );
				
				$i = 0;
				
				while ( $loop->have_posts() ) : $loop->the_post();
					$i++;
				?>
				<div class="noticia <?php if ($i == 4){ echo "ultimo";}?>">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="esquerda">
						<?php the_post_thumbnail('HomeNoticias'); ?></a>
					<div class="direita">
						<h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
						<?php the_excerpt(); ?>
					</div>
				</div>
				<div class="divisor m-bottom15"></div>
				<?php
				endwhile;
				unset($i);
				?>
			</div>
			<div id="home_sidebar" class="m-top7 direita">
				<?php dynamic_sidebar('Home Sidebar'); ?>
			</div>
			<div class="clear"></div>
			<div id="galerias" class="esquerda">
				<div id="g_video" class="esquerda">
					<h2>Últimos Vídeos</h2>
					<div class="divisor m-bottom15"></div>
					<?php 
					$args = array( 'post_type' => 'video', 'numberposts' => 2, 'posts_per_page' => 2,'orderby' => 'date', 'order' => 'DESC' );
					$loop = new WP_Query( $args );
					
					$i = 0;
					
					while ( $loop->have_posts() ) : $loop->the_post();
					$i++;
					?>
					<div class="video esquerda relative video<?php echo $i; ?>"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><span class="icon icon-camera absolute"></span><?php the_post_thumbnail('Archive'); ?></a>
						<h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
					</div>
					<?php
					//}
					endwhile;
					unset($i);
					?>
				</div>
				<div id="g_photo" class="esquerda">
					<h2>Últimos Álbuns de Fotos</h2>
					<div class="divisor m-bottom15"></div>
					<?php 
					$args = array( 'post_type' => 'album', 'numberposts' => 2, 'posts_per_page' => 2, 'orderby' => 'date', 'order' => 'DESC' );
					$loop = new WP_Query( $args );
					
					$i = 0;
					
					while ( $loop->have_posts() ) : $loop->the_post();
					$i++;
					?>
					<div class="album esquerda relative album<?php echo $i; ?>"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><span class="icon icon-camera-2 absolute"></span><?php the_post_thumbnail('Archive'); ?></a>
						<h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
					</div>
					<?php
					//}
					endwhile;
					unset($i);
					?>
				</div>
			</div>
<?php get_footer(); ?>