<?php
/*
Template Name: Header
*/
?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
<meta charset="utf-8">
<title>
<?php 

$titulo =  "";

if (is_single()) $titulo = get_the_title();
if (is_page()) $titulo = get_the_title();
if (is_front_page()) $titulo = get_bloginfo('description');
if (is_category()) $titulo = single_cat_title('', false);
if (is_tag()) $titulo = single_tag_title(); 
if (is_post_type_archive()) $titulo = get_post_type();
if (is_search()) $titulo = "Sua busca: ". get_search_query();
if (is_paged()) $pageNumber = (get_query_var('paged')) ? get_query_var('paged') : 1;

$blog_title = get_bloginfo( 'name' );

if (is_paged()) {
	$titulo = $blog_title .' | '. ucfirst($titulo) .', página '. $pageNumber ;
}else {
	$titulo = $blog_title .' | '. ucfirst($titulo);
}

echo $titulo;

?>
</title>
<?php
	wp_head();
?>
<!--[if lte IE 7]><script src="<?php bloginfo( 'stylesheet_directory' ); ?>/lte-ie7.js?v=3"></script><![endif]-->

<link rel="shortcut icon" href="<?php bloginfo( 'stylesheet_directory' ); ?>/images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" href="<?php bloginfo( 'stylesheet_directory' ); ?>/images/ico/apple-touch-icon-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php bloginfo( 'stylesheet_directory' ); ?>/images/ico/apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php bloginfo( 'stylesheet_directory' ); ?>/images/ico/apple-touch-icon-114x114-precomposed.png">
</head>
<body  <?php body_class(); ?>>
<div id="header">
	<div class="bg_logo">
		<div class="wrap centro">
			<div id="logo_busca">
				<div id="logo" class="esquerda">
					<a href="/" title="Igreja Adventista do Sétimo Dia || Central de Campinas"><img src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/images/logo_topo.png" alt="IATAI - Institulo Adventista Transamazônico" /></a></div>
				<div id="busca" class="direita">
				<?php get_search_form(); ?>
				</div>
			</div>
			<div class="clear"></div>
			<div id="menu_principal">
				<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'main_nav', 'items_wrap' => '<ul id="%1$s" class="menu-superior esquerda">%3$s</ul>', ) ); ?>
				<div id="redes_sociais1" class="direita m-top10">
					<a href="<?php call_twitterLink();?>" class="icon1 icon-twitter-2" target="_blank"></a>
					<a href="<?php call_facebookLink();?>" class="icon1 icon-facebook" target="_blank"></a>
					<a href="<?php call_youtubeLink();?>" class="icon1 icon-youtube" target="_blank"></a>
					<a href="<?php bloginfo('rss2_url'); ?>" class="icon1 icon-feed" target="_blank"></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="conteudo">
	<div class="wrap centro">
