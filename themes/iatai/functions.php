<?php
add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );
remove_action('wp_head', 'wp_generator');

// Função p/ inclusão dos CSSs no thema
function user_files(){
   if (!is_admin()) {
	   $theme  = get_theme( get_current_theme() );
	   wp_register_style('google-font', 'http://fonts.googleapis.com/css?family=Cantora+One',false,$theme['Version'],'all');
	   wp_register_style('webfont', get_template_directory_uri().'/css/style.css',false,$theme['Version'],'all');
	   wp_register_style('base', get_template_directory_uri().'/css/css.css',false,$theme['Version'],'all'); 
	   wp_register_style('ie', get_template_directory_uri().'/css/ie.css',false,$theme['Version'],'all'); 
	   $GLOBALS['wp_styles']->add_data( 'ie-style', 'conditional', 'IE' ); 
	   wp_enqueue_style( 'google-font' );
	   wp_enqueue_style( 'webfont' );
	   wp_enqueue_style( 'base' );
	   wp_enqueue_style( 'ie' );
   }
}
add_action('wp_enqueue_scripts','user_files');

add_image_size( 'HomeNoticias', 170, 100, TRUE );
add_image_size( 'Archive', 220, 130, TRUE );
add_image_size( 'Galerias', 220, 170, TRUE );
add_image_size( 'HomeFotoVideo', 310, 200, TRUE );

require_once('lib/cpt-video.php');
require_once('lib/cpt-album.php');
require_once('lib/funcoes.php');

function addmenus() {
	register_nav_menus( array( 'main_nav' => 'Menu Principal'));
}
add_action( 'init', 'addmenus' );

add_filter('excerpt_length', 'my_excerpt_length');
function my_excerpt_length($length) {
	return 35;
}

function myfeed_request($qv) {
	if (isset($qv['feed']) && !isset($qv['post_type']))
		$qv['post_type'] = array('post', 'video', 'gallery');
	return $qv;
}
add_filter('request', 'myfeed_request');

register_sidebar(array(
	'name'=>'Twitter Home',
	'id' => 'twitter-home',
	'description' => 'Colocar aqui o widget referente twitter no topo.',
	'before_widget' => '',
	'after_widget' => '',
	'before_title'=> '',
	'after_title'   => ''
));

register_sidebar(array(
	'name'=>'Slider',
	'id' => 'slider',
	'description' => 'Colocar aqui o widget referente ao Slider. Tamanho da imagem: 928px X 320px.',
	'before_widget' => '',
	'after_widget' => '',
	'before_title'=> '',
	'after_title'   => ''
));

register_sidebar(array(
	'name'=>'VideoDestaque',
	'id' => 'VideoDestaque',
	'description' => 'Colocar aqui o embed do vídeo.',
	'before_widget' => '',
	'after_widget' => '',
	'before_title'=> '',
	'after_title'   => ''
));

register_sidebar(array(
	'name'=>'Home Sidebar',
	'id' => 'home-sidebar',
	'description' => 'Largura máxima: 290px;',
	'before_widget' => '<div class="home-sidebar m-bottom30">',
	'after_widget' => '</div>',
	'before_title'=> '',
	'after_title'   => ''
));

register_sidebar(array(
	'name'=>'Sidebar - Universal',
	'id' => 'sidebar-universal',
	'description' => 'Colocar aqui os widgets que deverão aparecer na página de listagem das Categorias e Tags',
	'before_widget' => '<div class="widget m-bottom20">',
	'after_widget' => '</div>',
	'before_title'=> '<h2 class="titulo_sidebar">',
	'after_title'   => '</h2><div class="divisor m-bottom10"></div>'
));

register_sidebar(array(
	'name'=>'Sidebar - Resultado de Busca',
	'id' => 'sidebar-search',
	'description' => 'Colocar aqui os widgets que deverão aparecer na página do resultado de Busca',
	'before_widget' => '<div class="widget m-bottom20">',
	'after_widget' => '</div>',
	'before_title'=> '<h2 class="titulo_sidebar">',
	'after_title'   => '</h2><div class="divisor m-bottom10"></div>'
));

register_sidebar(array(
	'name'=>'Sidebar - Páginas',
	'id' => 'sidebar-pages',
	'description' => 'Colocar aqui os widgets que deverão aparecer na página de listagem das Categorias e Tags',
	'before_widget' => '<div class="widget m-bottom20">',
	'after_widget' => '</div>',
	'before_title'=> '<h2 class="titulo_sidebar">',
	'after_title'   => '</h2><div class="divisor m-bottom10"></div>'
));

register_sidebar(array(
	'name'=>'Sidebar - Categorias/Tags',
	'id' => 'sidebar-archive',
	'description' => 'Colocar aqui os widgets que deverão aparecer na página de listagem das Categorias e Tags',
	'before_widget' => '<div class="widget m-bottom20">',
	'after_widget' => '</div>',
	'before_title'=> '<h2 class="titulo_sidebar">',
	'after_title'   => '</h2><div class="divisor m-bottom10"></div>'
));

register_sidebar(array(
	'name'=>'Sidebar - Postagem',
	'id' => 'sidebar-single',
	'description' => 'Colocar aqui os widgets que deverão aparecer na página de listagem das Categorias e Tags',
	'before_widget' => '<div class="widget m-bottom20">',
	'after_widget' => '</div>',
	'before_title'=> '<h2 class="titulo_sidebar">',
	'after_title'   => '</h2><div class="divisor m-bottom10"></div>'
));

register_sidebar(array(
	'name'=>'Sidebar - Vídeos',
	'id' => 'sidebar-videos',
	'description' => 'Colocar aqui os widgets que deverão aparecer na página de listagem das Categorias e Tags',
	'before_widget' => '<div class="widget m-bottom20">',
	'after_widget' => '</div>',
	'before_title'=> '<h2 class="titulo_sidebar">',
	'after_title'   => '</h2><div class="divisor m-bottom10"></div>'
));

register_sidebar(array(
	'name'=>'Sidebar - Álbuns',
	'id' => 'sidebar-albuns',
	'description' => 'Colocar aqui os widgets que deverão aparecer na página de listagem das Categorias e Tags',
	'before_widget' => '<div class="widget m-bottom20">',
	'after_widget' => '</div>',
	'before_title'=> '<h2 class="titulo_sidebar">',
	'after_title'   => '</h2><div class="divisor m-bottom10"></div>'
));
