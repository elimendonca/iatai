<?php
/*
Template Name: Footer
*/
?>
</div>
</div>
<div class="clear"></div>
<div id="rodape">
	<div class="wrap centro">
		<div class="esquerda">
			<div id="copyright">
				<p>Copyright © 2012 - Todos os Direitos Reservados<br />
					IATAI - Instituto Adventista Transamazônico</p>
			</div>			
		</div>
		<div class="direita">
			<div id="redes_sociais2" class="m-bottom20">
				<a href="<?php call_twitterLink();?>" class="icon1 icon-twitter-2" target="_blank"></a>
				<a href="<?php call_facebookLink();?>" class="icon1 icon-facebook" target="_blank"></a>
				<a href="<?php call_youtubeLink();?>" class="icon1 icon-youtube" target="_blank"></a>
				<a href="<?php bloginfo('rss2_url'); ?>" class="icon1 icon-feed" target="_blank"></a>
			</div>
		</div>
	</div>
</div>
<?php wp_footer(); ?>
</body>
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js.js?v=3"></script>
</html>