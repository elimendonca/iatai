<?php //register video custom post type
add_action( 'init', 'register_cpt_video' );
function register_cpt_video() {

    $labels = array( 
        'name' => _x( 'Vídeos', 'videos' ),
        'singular_name' => _x( 'Vídeo', 'video' ),
        'add_new' => _x( 'Adicionar Novo', 'video' ),
        'add_new_item' => _x( 'Adicionar Novo Vídeo', 'video' ),
        'edit_item' => _x( 'Editar Vídeo', 'video' ),
        'new_item' => _x( 'Novo Vídeo', 'video' ),
        'view_item' => _x( 'Visualizar Vídeo', 'video' ),
        'search_items' => _x( 'Buscar Vídeos', 'video' ),
        'not_found' => _x( 'Nenhum vídeo encontrado', 'video' ),
        'not_found_in_trash' => _x( 'Nenhum vídeo encontrado na lixeira', 'video' ),
        'parent_item_colon' => _x( 'Parent Vídeo:', 'video' ),
        'menu_name' => _x( 'Vídeos', 'video' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        
        'supports' => array( 'title', 'editor', 'thumbnail' ),
        
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        
        //'menu_icon' => 'images/ico/apple-touch-icon-72x72-precomposed.png',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );
	register_post_type( 'video' , $args );
}

add_action("admin_init", "admin_init");
function admin_init(){
	add_meta_box("videoembed_meta", "Coloque abaixo o embed do vídeo", "videoembed_meta", "video", "normal", "low");
}

function videoembed_meta(){
	global $post;
	$custom = get_post_custom($post->ID);
	$videoembed_meta = $custom["videoembed_meta"][0];
	?>
	<textarea name="videoembed_meta" rows="3" style="width: 100%;"><?php echo $videoembed_meta; ?></textarea>
	<?php
}

add_action('save_post', 'save_link');
function save_link(){
	global $post;
	update_post_meta($post->ID, "videoembed_meta", $_POST["videoembed_meta"]);
}

add_filter("manage_edit-video_columns", "video_edit_columns"); 
function video_edit_columns($columns){
  $columns = array(
    "cb" => "<input type=\"checkbox\" />",
	//"post-thumbnails(AdminListThumb)" => "Featured",
    "title" => "Título do vídeo",
	"date" => "Data de publicação"
  );
  return $columns;
}

?>