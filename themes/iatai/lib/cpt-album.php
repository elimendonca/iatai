<?php //register album custom post type
add_action( 'init', 'register_cpt_album' );
function register_cpt_album() {

    $labels = array( 
        'name' => _x( 'Álbuns de fotos', 'albuns' ),
        'singular_name' => _x( 'Álbum', 'album' ),
        'add_new' => _x( 'Adicionar', 'album' ),
        'add_new_item' => _x( 'Adicionar novo álbum', 'album' ),
        'edit_item' => _x( 'Editar álbum', 'album' ),
        'new_item' => _x( 'Novo álbum', 'album' ),
        'view_item' => _x( 'Visualizar álbuns', 'album' ),
        'search_items' => _x( 'Buscar álbuns', 'album' ),
        'not_found' => _x( 'Nenhum álbum encontrada', 'album' ),
        'not_found_in_trash' => _x( 'Nenhum álbum encontrado na lixeira', 'album' ),
        'parent_item_colon' => _x( 'Parent álbum:', 'album' ),
        'menu_name' => _x( 'Álbum de fotos', 'album' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        
        'supports' => array( 'title', 'editor', 'thumbnail' ),
        
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        
        //'menu_icon' => 'images/ico/apple-touch-icon-72x72-precomposed.png',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );
	register_post_type( 'album' , $args );
}

add_action("admin_init", "admin_init2");
function admin_init2(){
	add_meta_box("albumembed_meta", "Cole abaixo o link completo do álbum do Facebook (obrigatório: álbum de uma FANPAGE)", "albumembed_meta", "album", "normal", "low");
}

function albumembed_meta(){
	global $post;
	$custom = get_post_custom($post->ID);
	$albumembed_meta = $custom["albumembed_meta"][0];
	?>
	<textarea name="albumembed_meta" rows="3" style="width: 100%;"><?php echo $albumembed_meta; ?></textarea>
	<?php
}

add_action('save_post', 'save_link2');
function save_link2(){
	global $post;
	update_post_meta($post->ID, "albumembed_meta", $_POST["albumembed_meta"]);
}

add_filter("manage_edit-album_columns", "album_edit_columns"); 
function album_edit_columns($columns){
  $columns = array(
    "cb" => "<input type=\"checkbox\" />",
	//"post-thumbnails(AdminListThumb)" => "Featured",
    "title" => "Título do álbum",
	"date" => "Data de publicação"
  );
  return $columns;
}

?>