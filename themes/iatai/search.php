<?php
/*
Template Name: Search Result
*/
?>
<?php get_header(); ?>
<div id="conteudo2">
<div id="r_busca" class="esquerda clear">
	<?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>'); } ?>

	<h2 class="titulo m-top30">Resultados para <strong><?php the_search_query(); ?></strong></h2>
	<div class="divisor m-bottom20 m-top5"></div>
	<?php 
	
	while ( have_posts() ) : the_post(); ?>
		<div class="item clear m-bottom20">
			<?php if (has_post_thumbnail()) { ?><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="esquerda thumbnail"><?php the_post_thumbnail('Archive'); ?></a> 
			<div class="direita posts">
			<?php } else { ?>
			<div class="direita"> <?php } ?>
				<h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
				<div class="divisor3 m-top3 m-bottom2"></div>
				<div class="meta"><?php the_date(); ?></div>	
				<p class="excerpt"><?php the_excerpt(); ?></p>	
			</div>
		</div>
		<div class="divisor m-bottom20 m-top5"></div>	
		<?php endwhile;?>
	<?php if(function_exists('wp_pagenavi')) { ?><div id="paginacao" class="m-top10 clear border-sup"><span class="dleft"><?php wp_pagenavi(); ?> </span></div><?php } ?>
</div>	
<?php get_sidebar(); ?>
<?php get_footer(); ?>
