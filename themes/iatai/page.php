<?php
/*
Template Name: Single
*/
?>
<?php get_header(); ?>
<div id="conteudo2">
<div id="conteudo_interno" class="esquerda">
	<?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>'); } ?>

	<?php while ( have_posts() ) : the_post(); ?>
	<h2 class="titulo m-top30">
		<?php the_title(); ?>
	</h2>
	<div class="share_meta m-top10 m-bottom15">
		<div class="share esquerda">
			<?php if(function_exists('kc_add_social_share')) kc_add_social_share(); ?>
		</div>
		<div class="meta direita">
			<?php the_date();?>
		</div>
	</div>
	<div class="post clear">
		<?php the_content(); ?>
		<?php if (has_category() || has_term()) {?>
		<div class="share_meta taxonomia m-top10 m-bottom15 clear">
			<div class="share esquerda"> <span class="icon-tag"></span>
				<?php the_category(', '); ?>
			</div>
			<div class="meta direita">
				<?php the_tags('');?>
				<span class="icon-tag-2"></span> </div>
		</div>
		<?php } ?>
		<?php endwhile; ?>
	</div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
