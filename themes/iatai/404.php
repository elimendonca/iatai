<?php
/*
Template Name: Erro 404
*/
?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
<meta charset="<?php get_bloginfo( 'charset' ); ?>" />
<title>IASD Central de Campinas - Erro 404
</title>
<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_directory' ); ?>/css/style.css?v=3">
<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_directory' ); ?>/css/css.css?v=3">

<!--[if lte IE 7]><script src="<?php bloginfo( 'stylesheet_directory' ); ?>/lte-ie7.js?v=3"></script><![endif]-->
</head>
<body  <?php body_class(); ?>>
<div id="erro" class="centro">
<h2 class="icon-cross">.404</h2><h4>Desculpe, não foi possível encontrar esse link</h4>
<a href="/">ir para a página principal</a>
</div>

</body>
</html>