<div id="sidebar" class="direita">
	<?php 
		if 	(is_single())	:  	dynamic_sidebar('sidebar-single');	endif;
		if 	(is_category())	: 	dynamic_sidebar('sidebar-archive');	endif;
		if 	(is_tag())		: 	dynamic_sidebar('sidebar-archive');	endif;
		if 	(is_page())		: 	dynamic_sidebar('sidebar-pages'); 	endif;
		if 	(is_search())	:	dynamic_sidebar('sidebar-search');	endif;
		if 	(is_post_type_archive('video')) : dynamic_sidebar('sidebar-videos'); endif;
		if 	(is_post_type_archive('album')) : dynamic_sidebar('sidebar-albuns'); endif;
	?>
	<?php dynamic_sidebar('sidebar-universal'); ?>
</div>