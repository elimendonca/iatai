<?php
/*
 * Plugin Name: Grinning Gecko Twitter
 * Version: 1.1.1
 * Plugin URI: http://grinninggecko.com/wordpress-gg-twitter-widget/
 * Description: Grinning Gecko Twitter Widget
 * Author: Garth Gutenberg
 * Author URI: http://grinninggecko.com/
 */
class GgTwitterWidget extends WP_Widget {

	/**
	* Declares the GgTwitterWidget class.
	*
	*/
	function GgTwitterWidget( $widget = true ) {
		if ( $widget ) {
			$widget_ops = array(
				'classname' => 'widget_gg_twitter',
				'description' => __( 'Grinning Gecko Twitter Widget' )
			 );
			$control_ops = array(
				'width' => 300,
				'height' => 300
			 );
			$this->WP_Widget( 'ggtwitter', __( 'GG Twitter' ), $widget_ops, $control_ops );
		}
	}

	function shortcode( $instance ) {
		ob_start( );
		$ggtwitter = new GgTwitterWidget( false );
		$ggtwitter->widget( array(
			'element' => 'div',
			'before_widget' => '',
			'after_widget' => ''
		 ), $instance );
		return ob_get_flush( );
		unset( $ggtwitter );
	}

	function load_file_from_url( $url ) {
		$curl = curl_init();
		curl_setopt( $curl, CURLOPT_URL, $url );
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLOPT_REFERER, get_home_url() );
		$str = curl_exec( $curl );
		curl_close( $curl );
		return $str;
	}

	function load_xml_from_url( $url ) {
		return simplexml_load_string( $this->load_file_from_url( $url ) );
	}

	/**
	* Displays the Widget
	*
	* @todo Check if the current twitter handle is the same as the cached (transient, option) handle and refresh cache if different
	*/
	function widget( $args, $instance ) {
		extract( $args );

		if ( empty( $element ) )
			$element = 'div';

		if ( empty( $before_title ) )
			$before_title = '<h3>';

		if ( empty( $after_title ) )
			$after_title = '</h3>';

		if ( empty( $instance['title'] ) )
			$title = '';
		else
			$title = $instance['title'];

		if ( empty( $instance['username'] ) )
			$username = '';
		else
			$username = $instance['username'];

		if ( empty( $instance['max_tweets'] ) )
			$max_tweets = 3;
		else
			$max_tweets = ( int ) $instance['max_tweets'];

		if ( empty( $instance['view_all_html'] ) )
			$view_all_html = '?>View All Tweets';
		else
			$view_all_html = '?>' . $instance['view_all_html'];

		if ( empty( $instance['time_placement'] ) )
			$time_placement = 'before';
		else
			$time_placement = $instance['time_placement'];

		if ( empty( $instance['time_format'] ) )
			$time_format = 'D M jS';
		else
			$time_format = $instance['time_format'];

		if ( empty( $instance['cache_expiration'] ) )
			$cache_expiration = 10 * 60;
		else
			$cache_expiration = $instance['cache_expiration'] * 60;

		if ( empty( $instance['debug'] ) )
			$debug = false;
		else
			$debug = (bool) $instance['debug'];

		// Before the widget
		echo $before_widget;

		if ( $debug ) {
			$debug_output = '';
			$debug_output .= '<style>';
			$debug_output .= '.gg_twitter_debug{width:100%;overflow:scroll;background-color:white;color:black;font-family:monospace;text-shadow:none;font-size:10px;}' . "\n";
			$debug_output .= '</style>';
			$debug_output .= '<textarea class="gg_twitter_debug" readonly="readonly" rows="20" onclick="this.focus();this.select();">GG TWITTER DEBUG' . "\n\n";
			$debug_output .= 'WP Version: ' . get_bloginfo( 'version' ) . "\n\n";
			$debug_output .= 'PHP Version: ' . phpversion() . "\n\n";
			$debug_output .= 'GG Twitter Version: ' . $instance['version'] . "\n\n";
			if ( ! function_exists( 'simplexml_load_file' ) ) {
				$debug_output .= 'SimpleXML is unavailable.' . "\n\n";
			}
			$debug_output .= '$args:' . "\n";
			$debug_output .= htmlspecialchars(print_r($args, true)) . "\n\n";
			$debug_output .= '$instance:' . "\n";
			$debug_output .= htmlspecialchars(print_r($instance, true)) . "\n\n";
		}

		//delete_transient( $widget_id );
		//delete_option( $widget_id );

		// Get the transient
		$xml = get_transient( $widget_id );
		if ( false === $xml ) {
			$source = 'http://api.twitter.com/1/statuses/user_timeline.rss?screen_name=' . $username;
			//$source = 'http://localhost/user_timeline.rss';  // Debug - Error XML

			if ( $debug ) {
				$debug_output .= '$source: ' . $source . "\n\n";
			}

			// Get the Twitter RSS
			$xml = $this->load_xml_from_url( $source );
			if ( $xml === false ) {
				// Failed completely
				if ( $debug ) {
					$debug_output .= '$xml === false' . "\n\n";

					if ( ! function_exists( 'curl_exec' ) )
						$debug_output .= 'curl is not installed' . "\n\n";

					if ( ! function_exists( 'simplexml_load_string' ) )
						$debug_output .= 'simplexml is not installed' . "\n\n";
				}

				// Fall back to the option
				$xml = json_decode( get_option( $widget_id ) );
			}
			elseif ( ! isset( $xml->channel ) || ! isset( $xml->channel->item ) ) {
				// Error; XML is not as expected
				if ( $debug )
					$debug_output .= '$xml->channel not set' . "\n\n";

				if ( isset( $xml->error ) ) {
					if ( $debug )
						$debug_output .= '$last_error:' . "\n" . htmlspecialchars(print_r($xml, true)) . "\n\n";

					$last_error = array(
						'time' => current_time('mysql'),
						'msg' => (string) $xml->error
					);
					update_option( 'gg_twitter_last_error', json_encode( $last_error ) );

					// Fall back to the option
					$xml = json_decode( get_option( $widget_id ) );
				}
			}
			else {
				// Success!
				if ( $debug )
					$debug_output .= '$xml retrieved from Twitter' . "\n\n";

				$json_xml = json_encode( $xml );

				// Update the transient
				set_transient( $widget_id, $json_xml, $cache_expiration );

				// Update the option
				update_option( $widget_id, $json_xml );
			}
		}
		else {
			if ( $debug )
				$debug_output .= '$xml retrieved from transient' . "\n\n";

			$xml = json_decode( $xml );
		}

		if ( $debug ) {
			$debug_output .= '$xml:' . "\n" . htmlspecialchars(print_r($xml, true)) . "\n\n";
			$debug_output .= '</textarea>';
			echo $debug_output;
		}

		?>
		<<?php echo $element; ?> class="gg-twitter widget-container">
			<?php if ( $title != '' ) { ?>
				<?php echo $before_title; ?><?php echo $title; ?><?php echo $after_title; ?>
			<?php } ?>
			<div class="tweets">
			<?php

			if ( $max_tweets > count ( $xml->channel->item ) )
				$max_tweets = count ( $xml->channel->item );

			for ( $i = 0; $i < $max_tweets; $i++ ) {
				if ( 1 === count( $xml->channel->item ) )
					$item = $xml->channel->item;
				else
					$item = $xml->channel->item[$i];
				$tweet_arr = explode( ':', $item->title, 2 );
				$tweeted_by = trim( $tweet_arr[0] );
				$tweet_contents = trim( $tweet_arr[1] );
				$tweet_date = date( $time_format, strtotime( $item->pubDate ) );
				// Parse/Link URLs
				$tweet_contents = preg_replace( '/http(\S+)/', '<a class="tweet-link" target="_blank" href="http$1">http$1</a>', $tweet_contents );
				// Parse/Link Email Addresses
				//$tweet_contents = preg_replace( '/\b([A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})\b/', '<a class="tweet-email" href="mailto:$1">$1</a>', $tweet_contents );
				// Parse/Link Twitter Names
				$tweet_contents = preg_replace( '/@(\S+?)\b/', ' <a class="tweet-at" target="_blank" href="http://twitter.com/$1">@$1</a>', $tweet_contents );
				// Parse/Link Twitter Hashtags
				$tweet_contents = preg_replace( '/#(\S+?)\b/', ' <a class="tweet-hash" target="_blank" href="http://twitter.com/search?q=%23$1&src=hash">#$1</a>', $tweet_contents );
				?>
				<div class="tweet<?php
				if ( $i == 0 ) {
					echo ' first';
				}
				if ( $i == ( $max_tweets - 1 ) ) {
					echo ' last';
				}
				?>">
					<?php if ( $time_placement == 'before' ) { ?>
						<div class="tweet-date"><?php echo $tweet_date; ?></div>
					<?php } ?>
					<div class="tweet-by"><a target="_blank" href="http://twitter.com/<?php echo $tweeted_by; ?>">@<?php echo $tweeted_by; ?></a></div>
					<div class="tweet-contents"><?php echo $tweet_contents; ?></div>
					<?php if ( $time_placement == 'after' ) { ?>
						<div class="tweet-date"><?php echo $tweet_date; ?></div>
					<?php } ?>
				</div>
				<?php
			}
			?>
			</div>
			<div class="view-all-tweets"><a target="_blank" href="http://twitter.com/<?php echo $username; ?>"><?php eval( $view_all_html ); ?></a></div>
		</<?php echo $element; ?>>
		<?php

		// After the widget
		echo $after_widget;
	}

	/**
	* Saves the widgets settings.
	*
	*/
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( stripslashes( $new_instance['title'] ) );
		$instance['username'] = strip_tags( stripslashes( $new_instance['username'] ) );
		$instance['max_tweets'] = strip_tags( stripslashes( $new_instance['max_tweets'] ) );
		$instance['view_all_html'] = $new_instance['view_all_html'];
		$instance['time_placement'] = $new_instance['time_placement'];
		$instance['time_format'] = $new_instance['time_format'];
		$instance['cache_expiration'] = $new_instance['cache_expiration'];
		$instance['debug'] = isset( $new_instance['debug'] ) ? 1 : 0;
		$plugin_data = get_plugin_data( __FILE__, false );
		$instance['version'] = $plugin_data['Version'];


		return $instance;
	}

	/**
	* Creates the edit form for the widget.
	*
	*/
	function form( $instance ) {
		$instance = wp_parse_args( ( array ) $instance, array(
			'title'=>'',
			'username'=>'',
			'max_tweets'=>'3',
			'view_all_html'=>'',
			'time_placement' => '',
			'time_format' => '',
			'cache_expiration' => 10, // minutes
			'debug' => 0
		 ) );

		$title = htmlspecialchars( $instance['title'] );
		$username = htmlspecialchars( $instance['username'] );
		$max_tweets = htmlspecialchars( $instance['max_tweets'] );
		$view_all_html = htmlspecialchars( $instance['view_all_html'] );
		$time_placement = htmlspecialchars( $instance['time_placement'] );
		$time_format = htmlspecialchars( $instance['time_format'] );
		$cache_expiration = (int) $instance['cache_expiration']; // minutes
		$debug = $instance['debug'];
		$error = json_decode( get_option( 'gg_twitter_last_error' ) );

		// Output the options
		?>
		<p style="text-align:left;">
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">
				Title:
			</label>
			<input class="widefat" id="<?php
				echo $this->get_field_id( 'title' );
				?>" name="<?php
				echo $this->get_field_name( 'title' );
				?>" type="text" value="<?php
				echo $title;
				?>" />
		</p>
		<p style="text-align:left;">
			<label for="<?php echo $this->get_field_id( 'username' ); ?>">
				Twitter Username:
			</label>
			<input class="widefat" id="<?php
				echo $this->get_field_id( 'username' );
				?>" name="<?php
				echo $this->get_field_name( 'username' );
				?>" type="text" value="<?php
				echo $username;
				?>" />
		</p>
		<p style="text-align:left;">
			<label for="<?php echo $this->get_field_id( 'max_tweets' ); ?>">
				Max Tweets to Display:
			</label>
			<input class="widefat" id="<?php
				echo $this->get_field_id( 'max_tweets' );
				?>" name="<?php
				echo $this->get_field_name( 'max_tweets' );
				?>" type="text" value="<?php
				echo $max_tweets;
				?>" />
		</p>
		<p style="text-align:left;">
			<label for="<?php echo $this->get_field_id( 'view_all_html' ); ?>">
				HTML for the View All Link:
			</label>
			<input class="widefat" id="<?php
				echo $this->get_field_id( 'view_all_html' );
				?>" name="<?php
				echo $this->get_field_name( 'view_all_html' );
				?>" type="text" value="<?php
				echo $view_all_html;
				?>" />
		</p>
		<p style="text-align:left;">
			<label for="<?php echo $this->get_field_id( 'time_placement' ); ?>">
				Time Placement
			</label>
			<select class="widefat" id="<?php
				echo $this->get_field_id( 'time_placement' );
				?>" name="<?php
				echo $this->get_field_name( 'time_placement' );
				?>">
				<option name="before" value="before"<?php
					echo ( $time_placement == 'before' ? ' selected="selected"' : '' );
					?>>Before</option>
				<option name="after" value="after"<?php
					echo ( $time_placement == 'after' ? ' selected="selected"' : '' );
					?>>After</option>
				<option name="none" value="none"<?php
					echo ( $time_placement == 'none' ? ' selected="selected"' : '' );
					?>>None</option>
			</select>
		</p>
		<p style="text-align:left;">
			<label for="<?php echo $this->get_field_id( 'time_format' ); ?>">
				Time Format ( see <a href="http://php.net/manual/en/function.date.php" target="_blank">PHP: date</a> for reference ):
			</label>
			<input class="widefat" id="<?php
				echo $this->get_field_id( 'time_format' );
				?>" name="<?php
				echo $this->get_field_name( 'time_format' );
				?>" type="text" value="<?php
				echo $time_format;
				?>" />
		</p>
		<p style="text-align:left;">
			<label for="<?php echo $this->get_field_id( 'cache_expiration' ); ?>">
				Cache Expiration (in minutes):
			</label>
			<input class="widefat" id="<?php
				echo $this->get_field_id( 'cache_expiration' );
				?>" name="<?php
				echo $this->get_field_name( 'cache_expiration' );
				?>" type="text" value="<?php
				echo $cache_expiration;
				?>" />
		</p>
		<p style="text-align:left;">
			<input type="checkbox" id="<?php
				echo $this->get_field_id( 'debug' );
				?>" name="<?php
				echo $this->get_field_name( 'debug' );
				?>" value="<?php
				echo $debug;
				?>. '"<?php
				echo ( (bool) $debug == true ? ' checked="checked"' : '' );
				?> />
			<label for="<?php
				echo $this->get_field_id( 'debug' );
				?>">
				Enable Debug. If no tweets are displaying:
				<ol>
					<li>Check this box and click Save.</li>
					<li>Visit the front-end of your site and copy the debug information that appears above the widget.</li>
					<li>Paste it into an email and send it to me at <a href="mailto:garth@grinninggecko.com">garth@grinninggecko.com</a>,
						or paste it into my contact form at <a href="http://grinninggecko.com/contact/" target="_blank">http://grinninggecko.com/contact/</a>.
					</li>
				</ol>
			</label>
		</p>
		<p style="text-align:left;">
			<strong>Last Twitter API Error</strong>:<br />
			<?php echo isset( $error ) ? $error->time . ' : ' . $error->msg : 'No errors reported.'; ?>
		</p>
		<?php
	}

}// END class


/**
* Register widget.
*
* Calls 'widgets_init' action after the widget has been registered.
*/
function GgTwitterInit( ) {
	register_widget( 'GgTwitterWidget' );
}
add_action( 'widgets_init', 'GgTwitterInit' );

// Shortcode
add_shortcode( 'gg-twitter', array( 'GgTwitterWidget', 'shortcode' ) );

?>