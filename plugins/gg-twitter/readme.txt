=== GG Twitter ===
Contributors: ggutenberg
Donate Link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=5UG376CS9WFFA
Tags: widget, twitter
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 1.1.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Display a twitter feed in a widget.

== Description ==
Lets you display a twitter feed in a sidebar widget.
Caches last successful tweet retrieval and only updates if it successfully gets a new list from Twitter.
Practically every element has a unique class so that it's highly customizable via CSS.

There are various configuration options including:

* Title
* Twitter Username
* Max Tweets to Display
* HTML to override the View All link
* Time Placement (whether or not the timestamp should appear before or after the tweet)
* Time Format (uses PHP's date() function for formatting)

== Installation ==
1. Upload `gg-twitter.php` to the `/wp-content/plugins/` directory
2. Activate the Plugin through the 'Plugins' menu in WordPress
3. Install the new Widget through the 'Widgets' menu in WordPress
4. That's it.

== Frequently Asked Questions ==
If you have problems with tweets displaying, please check the Enable Debug option and follow the instructions.

== Screenshots ==
1. Widget configuration screen

== Upgrade Notice ==
First release, so no notices yet :)

== Changelog ==
1.1.1

* Additional debug info

1.1

* Fix to cache expiration

1.0.9

* Added transient support with a configurable lifetime, option fallback

1.0.8

* Using cURL instead of simplexml_load_file as the latter was failing when the xml contained an error node
* Added Last Twitter API Error displayed on widget (global error, not per-widget)

1.0.7

* Added debug facility
* Changed option name from 'twitter' to $widget_id to avoid conflicts with other plugins
* Added 'None' option to Time Placement
* Fixed php shorthand tag

1.0.6

* Fixed spacing issue with linking @username
* Added linking for #hashtags
* Removed parsing for email addresses

1.0.5

* Fixed an issue where an error was occurring if there was only one tweet available
* Fixed parsing of URLs, email addresses and twitter names inside a tweet

1.0.4

* Updated plugin URL and readme.txt
* Added screenshot

1.0.3

* Updated readme.txt

1.0.2

* Updated twitter rss url

1.0.1

* Changed from using is_array() to count() with twitter rss

1.0

* First release