<?php

/* Plugin Name: FBPhotos
 * Plugin URI: http://demomentsomtres.com
 * Description: Replace [fbphotos id="ALBUM_ID" txtfb="FACEBOOK BUTTOM TEXT"] with a link to the facebook album
 * Version: 1.0
 * Author: Marc
 * Author URI: http://demomentsomtres.com/catala/author/marc
 */


// Register the post code
add_shortcode('fbphotos', 'dmst_fbphotos_shortcode');

// The callback function that will replace [dmst-fbphotos]
function dmst_fbphotos_shortcode($attr) {
    $default = array(
        'id' => '',
        'txtfb' => '',
        'caption' => '',
    );
    $opcions = shortcode_atts($default, $attr);
    if ('' == $opcions['id']):
        return '';
    endif;

    $url = 'http://graph.facebook.com/' . $opcions['id'] . '/photos?limit=30';
    $json = json_decode(file_get_contents($url));
    if (isset($json->error)):
        return '';
    endif;
    //$classes = ('' == $opcions['class']) ? '' : ' class="' . $opcions['class'] . '" ';
    $resultat = '<div>';
    foreach ($json->data as $imatge):
        $resultat.='<div class="fbphoto">';
        $resultat.='<a href="' . $imatge->source . '" target="_blank" rel="lightbox235" style="background: url('. $imatge->images[5]->source .') center center; display: block;">';
        //$resultat.='<div><a href="'.  $imatge->link .'">asd</a></div>';
		// $resultat.='<img src="'. $imatge->images[6]->source. '" />';
        $resultat.='</a>';
        $resultat.='</div>';
    endforeach;
    $resultat.='<div style="clear:both;"></div><div class="fbphoto_facebook"><a href="http://www.facebook.com/media/set/?set=a.'. $opcions['id'] .'" target="_blank" class="btfacebook">'. $opcions['txtfb'] .'</a></div></div>';
    return $resultat;
}

?>